/*
 * utils.c
 *
 * Created: 05.04.2020 01:22:13
 *  Author: Marios
 */ 
#include <stdint.h>

static uint32_t tick;

static void inline inc_tick(void)
{
	tick += 1;
}

uint32_t get_tick(void)
{
	return tick;
}