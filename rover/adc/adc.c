/*
 * adc.c
 *
 * Created: 05.04.2020 01:34:11
 *  Author: Marios
 */ 
#include <avr/io.h>
#include "adc.h"


void adc_init(void)
{
	//ADMUX |= (1<<REFS1)|(1<<REFS0) //internal 2.56V ref
	//ADCSRA |= (1<<ADEN)|
}

void adc_select_channel(adc_channel_t channel)
{
	ADMUX &= ~(0b00011111); //clear ADMUX
	ADMUX |= channel;	
}