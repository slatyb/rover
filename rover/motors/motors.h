/*
 * motors.h
 *
 * Created: 04.04.2020 23:17:24
 *  Author: Marios
 */ 


#ifndef MOTORS_H_
#define MOTORS_H_

void motors_init(void);
void motor_right_cw(void);
void motor_right_ccw(void);
void motor_left_cw(void);
void motor_left_ccw(void);



#endif /* MOTORS_H_ */